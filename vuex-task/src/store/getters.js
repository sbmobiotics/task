export const doubleCounter = (state) => {
    return state.counter*2;
}

export const singleCounter = (state) => {
    return state.counter;
}

export const stringCounter = (state) => {
    return state.counter + " Clicks";
}