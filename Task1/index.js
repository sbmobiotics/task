let obj = {
    add: function() {
        let sum = 0;
        for (let i=0; i< arguments.length; i++) {
            sum += arguments[i];
        }
        return sum;
    },
    sub: function() {
        let subValue = 0;
        if(arguments.length) {
            subValue = arguments[0];
            for (let i=1; i< arguments.length; i++) {
                subValue -= arguments[i];
            }
        }
        return subValue;
    },
    mul: function() {
        let mulValue = 1;
        for (let i=0; i< arguments.length; i++) {
            mulValue *= arguments[i];
        }
        return mulValue;
    }
}
alert(obj.add());

alert(obj.add(1,2,3,-4));
alert(obj.sub(7,3,2,-1));
alert(obj.mul(4,2,3));

// Call by Value

let prevValue = 24;
function changeValue(newValue) {
    newValue += 1;
    alert(newValue);
}
changeValue(prevValue);
alert(prevValue);

// Call by Reference

let prevObj = {
    a: 56
}
function changeObj(newObj) {
    newObj.a += 1;
}
changeObj(prevObj);
alert(prevObj.a);

function copyObj(newObj) {
    let copy = Object.assign({}, newObj);
    console.log(copy.a);
    copy.a+=1;
    console.log(copy.a);
}
copyObj(prevObj);
console.log(prevObj.a);

let prevArray = [{a:1},{b:2},{c:3}];
function updateArray(refArray, value) {
   let copyArray = refArray.map(val=>val);
   copyArray.push(value);
   copyArray[0] = {a: 10};
   console.log(copyArray);
}
updateArray(prevArray, { d: 4 });
console.log(prevArray);


