// UpperCaseString Prototype

String.prototype.toUpperCaseString = function () {
   let  strArray = this.split('');
    for(let i in strArray) {
        if( strArray[i].charCodeAt(0) >=97 && strArray[i].charCodeAt(0)<=122 ) {
            strArray[i]=String.fromCharCode(strArray[i].charCodeAt(0)-32);
        }
        
    }
    return strArray.join('');
}

alert('abc@sechg'.toUpperCaseString());

// LowerCaseString Prototype

String.prototype.toLowerCaseString = function () {
    let  strArray = this.split('');
     for(let i in strArray) {
         if( strArray[i].charCodeAt(0) >=65 && strArray[i].charCodeAt(0)<=90 ) {
             strArray[i]=String.fromCharCode(strArray[i].charCodeAt(0)+32);
         }
         
     }
     return strArray.join('');
 }
 
 alert('ABC SCD'.toLowerCaseString());

 // Closure

 let makeCounter= function() {
     let count = 0;
      let increment = function() {
         return count++; // Closure to count variable.
     }
     return increment;
 }

 let counter = makeCounter();
 alert( counter() );
 alert( counter() );
 console.dir(makeCounter);

 // Reduce, Map and Filter

 let students = [
    { id:1, name:'Jay', age:20, house:'green' },
    { id:2, name:'Jayesh', age:30, house:'red' },
    { id:3, name:'Jack', age:14, house:'sunflower' },
 ]

 // Map
 let studentId = students.map(student => student.id);
 alert(studentId);

 // Reduce
 let oldest = students[0];
 let eldestStudent = students.reduce(function (oldest, student){
    return (oldest.age || 0) > student.age ? oldest : student;
 },{});
 alert(eldestStudent.age);

 // Filter
let green = students.filter(student => student.house == 'green');
let youngStudents = students.filter(student => student.age<30);
console.log(green[0].house);
youngStudents.map(student => alert(student.age));
 
